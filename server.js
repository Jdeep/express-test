import express from "express";

const app = express();

app.get("/", (_req, res) => {
  return res.json("API is working");
});

app.listen(5000, () => {
  console.log("API is running");
});
